const displayValue = document.querySelector(".display");
const buttonsContainer = document.querySelector(".buttons-container");

let operator = null;
let total = 0;
let initialValue = "0";

buttonsContainer.addEventListener("click", function (event) {
  onClickButton(event.target.innerText);
});

function onClickButton(value) {
  if (isNaN(parseInt(value))) {
    symbolHandler(value);
  } else {
    numberHandler(value);
  }
  render();
}

let numberHandler = (value) => {
  if (initialValue === "0") {
    initialValue = value;
  } else {
    initialValue += value;
  }
};

let symbolHandler = (value) => {
  switch (value) {
    case "C":
      initialValue = "0";
      total = 0;
      break;
    case "=":
      if (operator === null) {
        return;
      }
      operate(parseInt(initialValue));
      operator = null;
      initialValue = " " + total;
      total = 0;
      break;
    case "↩":
      if (initialValue.length === 1) {
        initialValue = "0";
      } else {
        initialValue = initialValue.substring(0, initialValue.length - 1);
      }
      break;
    default:
      caluclate(value);
      break;
  }
};

let caluclate = (value) => {
  const intInitialValue = parseInt(initialValue);
  if (total === 0) {
    total = intInitialValue;
  } else {
    operate(intInitialValue);
  }
  operator = value;
  initialValue = "0";
};

let operate = (intInitialValue) => {
  if (operator === "+") {
    total += intInitialValue;
  } else if (operator === "-") {
    total -= intInitialValue;
  } else if (operator === "✕") {
    total *= intInitialValue;
  } else {
    total /= intInitialValue;
  }
};

function render() {
  displayValue.textContent = initialValue;
}
